package org.droidtr.jelly;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import org.droidtr.jelly.utils.PrefsUtils;

public class editHome extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_homepage_edit);
        ((EditText) findViewById(R.id.homepage_edit_url)).setText(PrefsUtils.getHomePage(this));
    }
    public void ok(View w){
        String url = ((EditText) findViewById(R.id.homepage_edit_url)).getText().toString();
        if(url.isEmpty()){
            url=getResources().getString(R.string.default_home_page);
        }
        PrefsUtils.setHomePage(this, url);
        finish();
    }
    public void cancel(View w){
        finish();
    }
}
