/*
 * Copyright (C) 2017 The droidtr Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.droidtr.jelly;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.webkit.CookieManager;
import android.widget.Toast;

import org.droidtr.jelly.utils.PrefsUtils;

public class SettingsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);
    }

    public static class MyPreferenceFragment extends PreferenceFragment {

        @Override
        public void onCreate(Bundle savedInstance) {
            super.onCreate(savedInstance);
            addPreferencesFromResource(R.xml.settings);

            Preference homePage = findPreference("key_home_page");
            homePage.setSummary(PrefsUtils.getHomePage(getContext()));
            homePage.setOnPreferenceClickListener(preference -> {
                editHomePage(preference);
                return true;
            });

            Preference clearCookie = findPreference("key_cookie_clear");
            clearCookie.setOnPreferenceClickListener(preference -> {
                CookieManager.getInstance().removeAllCookies(null);
                Toast.makeText(getContext(), getString(R.string.pref_cookie_clear_done),
                        Toast.LENGTH_LONG).show();
                return true;
            });
        }

        private void editHomePage(Preference preference) {
            Intent i = new Intent(getContext(),editHome.class);
            startActivity(i);
        }
    }
}
